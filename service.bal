import ballerina/http;
import ballerina/io;

 type profile record {
    int id;
    string firstname;
    string lastname;
    string[] preferred_formats;
    json[] past_subjets;
};

profile[] students = [];


service /students on new http:Listener(9090) {



    resource function post addStd (@http:Payload profile newStd) returns json{

        students.push(newStd);

        return {"Added":" "+newStd.firstname+"was added successfully."};
        
    }

    resource function get getStd () returns profile[] {

        return students;
        
    }

    resource function put updateStd (int id, string newName) returns string{

        foreach int i in 0..< students.length() {
            if students[i].id === id {
               students[i].firstname = newName;
            }
        }

        return "Name was changed to "+newName+".";
        
    }





    resource function get sayHello(http:Caller caller, http:Request request) {

        // Send a response back to the caller.
        error? result = caller->respond("Hello Ballerina!");
        if (result is error) {
            io:println("Error in responding: ", result);
        }
    }
}
